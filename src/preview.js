/* global require, document */
import scrollToTop from './components/sdc-scroll-to-top';

require('../styles/preview.scss');

const components = document.querySelectorAll('.sdc-scroll-to-top');
for (let i = 0; i < components.length; i++) {
  scrollToTop(components[i]);
}
