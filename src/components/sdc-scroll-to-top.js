import scrollToTopController from './scroll-to-top-controller';
/**
 * Initialises the component when passed an element
 *
 * @param {HTMLNode} rootElement The component dom node
 */
export default function(rootElement) {
  if (!rootElement) {
    return false;
  }

  return scrollToTopController(rootElement);
}
