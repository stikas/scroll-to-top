/* global document */
/* eslint no-shadow: [2, { "hoist": "never" }] */
/* eslint-env es6 */

/**
 *
 * @param rootElement
 * @returns {boolean}
 */
export default function(rootElement) {
  if (!rootElement) {
    return false;
  }

  const scrollToTopButton = createScrollToTopButton(rootElement);

    /**
     *
     * @param rootElement
     * @returns {HTMLElement}
     */
  function createScrollToTopButton(rootElement) {
    const button = document.createElement('button');
    button.setAttribute('data-role', 'scroll-to-top-button');
    button.className = 'scroll-to-top-button';

    rootElement.appendChild(button);

    return button;
  }

  window.addEventListener('scroll', (e) => {
    e.preventDefault();

    if (window.scrollY > 1500) {
      makeButtonVisible(rootElement);
    } else {
      hideButton(rootElement);
    }
  });

  scrollToTopButton.addEventListener('click', (e) => {
    e.preventDefault();
    scrollToTop();
  });
}

/**
 *
 * @returns {Element | any}
 */
function scrollToTop() {
  window.scrollTo(0, 0);
}

/**
 *
 * @returns {Element | any}
 */
function makeButtonVisible(rootElement) {
  const buttonContainer = rootElement.querySelector('[data-role="scroll-to-top-button"]');
  buttonContainer.className = 'scroll-to-top-button--is-visible';
}

/**
 *
 * @returns {Element | any}
 */
function hideButton(rootElement) {
  const buttonContainer = rootElement.querySelector('[data-role="scroll-to-top-button"]');
  buttonContainer.className = 'scroll-to-top-button';
}

