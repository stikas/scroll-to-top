import component from '../../src/components/sdc-scroll-to-top';
import * as scrollToTopController from '../../src/components/scroll-to-top-controller';

describe('Scroll to top Component', () => {
  let parent;
  let scrollToTopControllerStub;

  beforeEach(() => {
    scrollToTopControllerStub = sinon.stub(scrollToTopController, 'default');
    parent = createDiv('parent');
    document.getElementsByTagName('body')[0].appendChild(parent);
  });

  afterEach(() => {
    scrollToTopControllerStub.restore();
    parent = null;
  });

  it('is a function', () => {
    expect(typeof component).to.equal('function');
  });

  it('does nothing if not passed the correct number of arguments', () => {
    const test = component();
    expect(test).to.be.false;
  });

  it('initialises the controller', () => {
    component(parent);
    expect(scrollToTopControllerStub).to.have.been.calledWith(
          parent
      );
  });
});

function createDiv(classNameValue) {
  const div = document.createElement('div');
  div.className = classNameValue;

  return div;
}
