import component from '../../src/components/scroll-to-top-controller';

describe('Show Scroll Button if scrollY is over 1500', () => {
  let parent;

  beforeEach(() => {
    parent = createDiv('parent');
    document.getElementsByTagName('body')[0].appendChild(parent);
  });

  afterEach(() => {
    parent = null;
  });

  it('is a function', () => {
    expect(typeof component).to.equal('function');
  });

  it('does nothing if not passed the correct number of arguments', () => {
    const test = component();
    expect(test).to.be.false;
  });

  it('should show the scroll to top button', () => {
    component(parent);

    global.scrollY = 1501;
    global.dispatchEvent(new Event('scroll'));

    const button = parent.querySelector('[data-role="scroll-to-top-button"]');

    expect(button.className).to.equal('scroll-to-top-button--is-visible');
  });

  it('should hide the scroll to top button', () => {
    component(parent);

    global.scrollY = 1499;
    global.dispatchEvent(new Event('scroll'));

    const button = parent.querySelector('[data-role="scroll-to-top-button"]');

    expect(button.className).to.equal('scroll-to-top-button');
  });

  it('should scroll to top', () => {
    component(parent);

    global.scrollTo = jest.fn();
    expect(global.scrollTo).not.toHaveBeenCalled();

    const button = parent.querySelector('[data-role="scroll-to-top-button"]');
    button.click.call(button);

    expect(global.scrollTo).toHaveBeenCalledWith(0, 0);
  });
});

function createDiv(classNameValue) {
  const div = document.createElement('div');
  div.className = classNameValue;

  return div;
}
