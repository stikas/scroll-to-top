# Toolbelt Component - Blackjack Sdc Scroll To Top


## Introduction
This project uses the [Blackjack Toolbelt](https://github.com/sky-uk/blackjack-toolbelt) as the build tool.

For help using the tool, see the [Blackjack Toolbelt](https://github.com/sky-uk/blackjack-toolbelt) project repository or run `blackjack --help` once installed.

## Preview
`tb preview`


## Installation

You can install this component using:

\`\`\`
npm install component-blackjack-sdc-scroll-to-top --save
\`\`\`


## Usage

## Linting
`tb lint`
`tb lint -f` will fix linting errors

## Testing
To test the component scripts, run this command:

`tb test`